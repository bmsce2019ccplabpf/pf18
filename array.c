#include<stdio.h>
void main()
{
	int a[20],n,small,big,i;
	int loc_big,loc_small;
	printf("Enter the number of elements to enter : ");
	scanf("%d",&n);
	printf("Enter the Elements : \n");
	for(i=0;i<n;i++)
	{
		scanf("%d",&a[i]);
	}
	small = a[0];
	big = a[0];
	for(i=0;i<n;i++)
	{
		if(a[i]<=small)
		{
			small = a[i];
			loc_small = i;
		}
		if(a[i]>=big)
		{
			big = a[i];
			loc_big = i;
		}
	}
	printf("Before swapping: \n");
	for(i=0;i<n;i++)
	{
		printf("%d\n",a[i]);
	}
	printf("Largest number = %d and location is = %d \n",big,loc_big);
	printf("Smallest number = %d and location is = %d \n",small,loc_small);
	int temp;
	temp = a[loc_small];
	a[loc_small] = a[loc_big];
	a[loc_big] = temp;
	printf("After swapping: \n");
	for(i=0;i<n;i++)
	{
		printf("%d\n",a[i]);
	}
}
	