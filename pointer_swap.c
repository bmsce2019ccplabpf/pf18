#include<stdio.h>
int swap(int *a,int *b);
int main()
{
int a,b;
printf("Enter the values of a and b:\n");
scanf("%d%d",&a,&b);
swap(&a,&b);
printf("The value of a and b after swaping is:\n");
printf("a=%d b=%d\n",a,b);
return 0;
}
int swap (int *a, int *b)
{
int temp;
temp=*a;
*a=*b;
*b=temp;
return 0;
}